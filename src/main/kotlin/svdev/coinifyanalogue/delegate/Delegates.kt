package svdev.coinifyanalogue.delegate

import io.holunda.camunda.bpm.data.CamundaBpmData
import io.holunda.camunda.bpm.data.reader.VariableReader
import org.camunda.bpm.engine.delegate.BpmnError
import org.camunda.bpm.engine.delegate.DelegateExecution
import org.camunda.bpm.engine.delegate.JavaDelegate
import org.camunda.bpm.engine.variable.Variables
import org.camunda.bpm.engine.variable.Variables.objectValue
import org.springframework.beans.factory.annotation.Autowired
import svdev.coinifyanalogue.camunda.data.CamundaVariables.INTERMEDIATE_AMOUNT
import svdev.coinifyanalogue.camunda.data.CamundaVariables.INVOICE_PAID
import svdev.coinifyanalogue.camunda.data.CamundaVariables.INVOICE_REQUEST
import svdev.coinifyanalogue.camunda.data.CamundaVariables.INVOICE_STATUS
import svdev.coinifyanalogue.camunda.data.CamundaVariables.RESPONSE_SENT
import svdev.coinifyanalogue.invoice.data.InvoiceRequest
import svdev.coinifyanalogue.invoice.data.InvoiceStatus
import svdev.coinifyanalogue.invoice.data.Ticker
import svdev.coinifyanalogue.invoice.services.InvoiceService
import svdev.coinifyanalogue.repositories.InvoiceRepository
import svdev.coinifyanalogue.tatum.services.WalletService
import java.math.BigDecimal
import javax.inject.Named

@Named("SendResponseToCallback")
class SendResponseToCallbackDelegate(@Autowired val invoiceService: InvoiceService) : JavaDelegate {
    override fun execute(execution: DelegateExecution?) {
        val reader: VariableReader = CamundaBpmData.reader(execution)
        val invoiceRequest = reader.get(INVOICE_REQUEST)
        val invoiceStatus = reader.get(INVOICE_STATUS)
        // val responseSent = invoiceService.sendResponseToCallback(invoiceRequest, invoiceStatus)
        RESPONSE_SENT.on(execution).set(true)

    }
}

@Named("SendAmountToPayoutWallet")
class SendAmountToPayoutWalletDelegate(
    @Autowired val walletService: WalletService,
    @Autowired val invoiceRepository: InvoiceRepository
) : JavaDelegate {
    override fun execute(execution: DelegateExecution?) {
        val reader: VariableReader = CamundaBpmData.reader(execution)
        val invoicePaid = reader.get(INVOICE_PAID)
        val invoiceFromBase =
            invoiceRepository.findByBusinessKey(execution?.businessKey!!)!!
        println("SEND AMOUNT TO PAYOUT WALLET")
        walletService.sendCurrencyToPayoutWallet(
            Ticker.valueOf(invoicePaid.currency),
            invoiceFromBase.amount,
            invoicePaid.to
        )
    }
}

@Named("Refund")
class RefundDelegate(
    @Autowired val invoiceService: InvoiceService,
    @Autowired val walletService: WalletService
) : JavaDelegate {
    override fun execute(execution: DelegateExecution?) {
        val reader: VariableReader = CamundaBpmData.reader(execution)
        val invoiceRequest = reader.get(INVOICE_REQUEST)
        val invoicePaid = reader.get(INVOICE_PAID)

        walletService.sendTransaction(
            invoiceRequest.customerAddress,
            Ticker.valueOf(invoicePaid.currency),
            "${invoiceRequest.amount}",
            invoicePaid.to
        )

        invoiceService.updateInvoiceStatus(execution?.businessKey!!, InvoiceStatus.CONFIRMED)
        INVOICE_STATUS.on(execution).set(InvoiceStatus.CONFIRMED)

    }
}

@Named("CheckAmount")
class CheckAmountDelegate(
    @Autowired val invoiceService: InvoiceService
) : JavaDelegate {
    override fun execute(execution: DelegateExecution?) {
        invoiceService.updateInvoiceStatus(execution?.businessKey!!, InvoiceStatus.CHECKING)

        val reader: VariableReader = CamundaBpmData.reader(execution)
        val invoiceRequest = reader.get(INVOICE_REQUEST)
        val invoicePaid = reader.get(INVOICE_PAID)
        val intermediateAmount =
            invoiceRequest.amount.minus(BigDecimal(invoicePaid.amount))

        if (BigDecimal.ZERO.compareTo(intermediateAmount) != 0) {
            INTERMEDIATE_AMOUNT.on(execution).set(intermediateAmount)
            execution.setVariable(
                "invoiceRequest",
                objectValue(
                    InvoiceRequest(
                        intermediateAmount.abs(),
                        invoiceRequest.currency,
                        invoiceRequest.customerAddress,
                        invoiceRequest.callback
                    )
                ).serializationDataFormat(Variables.SerializationDataFormats.JSON).create()
            )
            if (intermediateAmount > BigDecimal.ZERO) invoiceService.updateInvoiceStatus(
                execution.businessKey,
                InvoiceStatus.PENDING
            )
            throw BpmnError("TransactionIsNotCompleted")
        }

        INVOICE_STATUS.on(execution).set(InvoiceStatus.CONFIRMED)
        invoiceService.updateInvoiceStatus(execution.businessKey, InvoiceStatus.CONFIRMED)
    }
}

@Named("CancelInvoice")
class CancelInvoiceDelegate(@Autowired val invoiceService: InvoiceService) : JavaDelegate {
    override fun execute(execution: DelegateExecution?) {
        INVOICE_STATUS.on(execution).set(InvoiceStatus.CANCELED)
        invoiceService.updateInvoiceStatus(execution?.businessKey!!, InvoiceStatus.CANCELED)
    }
}