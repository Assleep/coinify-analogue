package svdev.coinifyanalogue.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import svdev.coinifyanalogue.invoice.data.InvoiceRequest
import svdev.coinifyanalogue.invoice.data.InvoiceResponse
import svdev.coinifyanalogue.invoice.services.InvoiceService

@RestController
@RequestMapping("/api/invoice")
class InvoiceController(@Autowired val invoiceService: InvoiceService) {
    @PostMapping("")
    fun invoice(@RequestBody invoiceRequest: InvoiceRequest): InvoiceResponse =
        invoiceService.sendInvoice(invoiceRequest)

}