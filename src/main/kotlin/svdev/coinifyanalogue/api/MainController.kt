package svdev.coinifyanalogue.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import svdev.coinifyanalogue.camunda.CamundaService
import svdev.coinifyanalogue.tatum.data.WebhookResponse

@RestController
@RequestMapping("/api")
class MainController(@Autowired val camundaService: CamundaService) {
    @PostMapping("/webhook")
    fun webhook(@RequestBody webhookResponse: WebhookResponse) {
        camundaService.sendMessageToCamundaProcess(webhookResponse)
    }
}