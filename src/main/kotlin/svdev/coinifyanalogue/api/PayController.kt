package svdev.coinifyanalogue.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import svdev.coinifyanalogue.invoice.entities.InvoiceEntity
import svdev.coinifyanalogue.pay.entities.PayoutEntity
import svdev.coinifyanalogue.pay.services.PayService

@RestController
@RequestMapping("/api/pay")
class PayController(@Autowired val payService: PayService){
    @GetMapping("/payins")
    fun getPayins(@RequestParam("callback") callback: String?): List<InvoiceEntity>{
        if(callback == null) return payService.getAllPayins()
        return payService.getPayins(callback)
    }

    @GetMapping("/payouts")
    fun getPayouts(@RequestParam("callback", required = true) callback: String): List<PayoutEntity> = payService.getPayouts(callback)

    @PostMapping("/payout")
    fun makePayout(@RequestBody payoutEntity: PayoutEntity){
        payService.makePayout(payoutEntity)
    }
}