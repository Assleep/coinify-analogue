package svdev.coinifyanalogue.pay.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import svdev.coinifyanalogue.invoice.data.Ticker
import svdev.coinifyanalogue.invoice.entities.InvoiceEntity
import svdev.coinifyanalogue.pay.entities.PayoutEntity
import svdev.coinifyanalogue.pay.repositories.PayoutRepository
import svdev.coinifyanalogue.repositories.InvoiceRepository
import svdev.coinifyanalogue.tatum.services.WalletService
import java.math.BigDecimal

@Service
class PayService(
    @Autowired val invoiceRepository: InvoiceRepository,
    @Autowired val payoutRepository: PayoutRepository,
    @Autowired val walletService: WalletService,
    @Value("\${tatum.payoutWalletAddress}") val payoutWalletAddress: String
) {
    fun getAllPayins(): List<InvoiceEntity> = invoiceRepository.findAll()
    fun getPayins(callback: String) = invoiceRepository.findAllByCallback(callback)
    fun makePayout(payout: PayoutEntity): Boolean {
        if (!canPayout(payout.callback, payout.amount)) return false
        walletService.sendTransaction(
            payout.toWallet,
            Ticker.valueOf(payout.currency),
            "${payout.amount}",
            "0x$payoutWalletAddress"
        )
        payoutRepository.save(payout)
        return true
    }

    fun getPayouts(callback: String): List<PayoutEntity> = payoutRepository.findAllByCallback(callback)

    private fun canPayout(callback: String, payoutAmount: BigDecimal): Boolean =getBalanceByCallback(callback) >= payoutAmount

    private fun getBalanceByCallback(callback: String): BigDecimal {
        val payins = getPayins(callback).filter {
            it.status.equals("CONFIRMED")
        }
        if (payins.isNotEmpty()) return payins.map { it.amount }
            .reduce { acc, amount -> acc + amount } - getPayoutAmount(callback)
        return BigDecimal.ZERO
    }

    fun getPayoutAmount(callback: String): BigDecimal {
        val payouts = payoutRepository.findAllByCallback(callback)
        if (payouts.isNotEmpty()) return payouts.map { it.amount }.reduce { acc, amount -> acc + amount }
        return BigDecimal.ZERO
    }
}