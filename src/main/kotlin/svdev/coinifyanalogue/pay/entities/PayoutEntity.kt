package svdev.coinifyanalogue.pay.entities

import java.math.BigDecimal
import java.time.Instant
import javax.persistence.*

@Entity
data class PayoutEntity(
    @Column
    val amount: BigDecimal,
    @Column
    val currency: String,
    @Column
    val toWallet: String,
    @Column
    val callback: String,
    @Column
    val date: Long = Instant.now().toEpochMilli(),
    @Id
    @GeneratedValue(
        strategy = GenerationType.IDENTITY
    )
    val id: Long? = null
)
