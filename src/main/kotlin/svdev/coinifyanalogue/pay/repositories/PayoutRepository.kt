package svdev.coinifyanalogue.pay.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import svdev.coinifyanalogue.pay.entities.PayoutEntity

interface PayoutRepository: JpaRepository<PayoutEntity, Long> {
    @Query("SELECT p FROM PayoutEntity p WHERE p.callback =:callback")
    fun findAllByCallback(@Param("callback") callback: String): List<PayoutEntity>
}