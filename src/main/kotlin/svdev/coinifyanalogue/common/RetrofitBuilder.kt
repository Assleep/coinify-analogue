package svdev.coinifyanalogue.common

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

object RetrofitBuilder {
    private val mapper = ObjectMapper().registerKotlinModule()
    fun getRetrofitBuilder(): Retrofit.Builder = Retrofit.Builder().addConverterFactory(JacksonConverterFactory.create(mapper))
}