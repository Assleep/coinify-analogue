package svdev.coinifyanalogue.common.entities

import javax.persistence.*

@Entity
data class WalletEntity(
    @Column
    val currency: String,
    @Column
    val xpub: String,
    @Column
    val account_id: String,
    @Column
    val address: String,
    @Column
    val privateKey: String,
    @Id
    @GeneratedValue(
        strategy = GenerationType.IDENTITY
    )
    val id: Long? = null
)
