package svdev.coinifyanalogue.tatum.data

data class TransactionRequest(
    val to: String,
    val currency: String,
    val fromPrivateKey: String,
    val amount: String
)
