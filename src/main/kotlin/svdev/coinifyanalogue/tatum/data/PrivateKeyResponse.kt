package svdev.coinifyanalogue.tatum.data

data class PrivateKeyResponse(val key: String)
