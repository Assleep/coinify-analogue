package svdev.coinifyanalogue.tatum.data

data class TatumWalletResponse(
    val xpub: String,
    val mnemonic: String
)
