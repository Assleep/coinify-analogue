package svdev.coinifyanalogue.tatum.data

data class AddressResponse(val address: String)