package svdev.coinifyanalogue.tatum.data

data class PrivateKeyRequest(
    val mnemonic: String,
    val index: Int
)
