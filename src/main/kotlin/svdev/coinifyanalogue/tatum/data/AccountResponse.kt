package svdev.coinifyanalogue.tatum.data

data class AccountResponse(
    val currency: String,
    val active: Boolean,
    val balance: Any,
    val frozen: Boolean,
    val accountingCurrency: String,
    val id: String
)
