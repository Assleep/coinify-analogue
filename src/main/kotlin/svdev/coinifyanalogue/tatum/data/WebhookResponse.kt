package svdev.coinifyanalogue.tatum.data

import spinjar.com.fasterxml.jackson.annotation.JsonProperty

class WebhookResponse(
    @JsonProperty("amount")
    val amount: String,
    @JsonProperty("currency")
    val currency: String,
    @JsonProperty("from")
    val from: String,
    @JsonProperty("to")
    val to: String
)
