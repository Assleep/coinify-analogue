package svdev.coinifyanalogue.tatum.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import svdev.coinifyanalogue.common.entities.WalletEntity
import svdev.coinifyanalogue.invoice.data.Ticker
import svdev.coinifyanalogue.repositories.WalletRepository
import svdev.coinifyanalogue.tatum.TatumApiInterface
import svdev.coinifyanalogue.tatum.TatumRetrofit
import svdev.coinifyanalogue.tatum.data.PrivateKeyRequest
import svdev.coinifyanalogue.tatum.data.TransactionRequest
import java.math.BigDecimal

@Service
class WalletService(
    @Autowired var walletRepository: WalletRepository,
    @Autowired val tatumRetrofit: TatumRetrofit,
    @Value("\${tatum.payoutWalletAddress}") val payoutWalletAddress: String
) {
    val tatumApi: TatumApiInterface = tatumRetrofit.tatumApiImpl

    private fun generateWallet(currency: Ticker): WalletEntity {
        val response = tatumApi.generateCurrencyWallet(currency.fullName, tatumRetrofit.api_key).execute().body()!!
        val walletAddress = generateAddress(currency, response.xpub)
        tatumApi.assignAddressToAccount(walletAddress, tatumRetrofit.api_key).execute()
        val walletEntity = WalletEntity(
            currency.name,
            response.xpub,
            tatumRetrofit.accountId,
            walletAddress,
            generatePrivateKey(currency, response.mnemonic)
        )
        walletRepository.save(walletEntity)
        return walletEntity
    }

    fun getAddress(currency: Ticker): String {
        val wallet = generateWallet(currency)
        return wallet.address
    }

    private fun generateAddress(currency: Ticker, xpub: String): String {
        val request = tatumApi.generateWalletAddress(currency.fullName, xpub, tatumRetrofit.api_key)
        return request.execute().body()?.address!!
    }

    private fun generatePrivateKey(currency: Ticker, mnemonic: String): String =
        tatumApi.generatePrivateKey(currency.fullName, PrivateKeyRequest(mnemonic, 1), tatumRetrofit.api_key).execute()
            .body()?.key!!

    fun sendTransaction(toAddress: String, currency: Ticker, amount: String, fromAddress: String): Int {
        val walletEntity = walletRepository.findByAddress(fromAddress)
        val transactionRequest = TransactionRequest(toAddress, currency.name, walletEntity.privateKey, amount)
        val response = tatumApi.sendTransaction(currency.fullName, transactionRequest, tatumRetrofit.api_key).execute()
        if (!response.isSuccessful && response.code() != 403) throw RuntimeException(response.errorBody()?.string())
        return response.code()
    }

    fun sendCurrencyToPayoutWallet(currency: Ticker, amount: BigDecimal, fromAddress: String) {
        sendTransaction("0x$payoutWalletAddress", currency, "$amount", fromAddress)
    }
}