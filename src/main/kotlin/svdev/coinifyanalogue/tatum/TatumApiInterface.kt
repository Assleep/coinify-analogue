package svdev.coinifyanalogue.tatum

import okhttp3.ResponseBody
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Service
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.http.*
import svdev.coinifyanalogue.common.RetrofitBuilder
import svdev.coinifyanalogue.tatum.data.*
import java.math.BigDecimal

@Service
class TatumRetrofit(
    @Value("\${tatum.accountId}")
    val accountId: String,
    @Value("\${tatum.apiKey}")
    val api_key: String,
    @Value("\${tatum.defaultTax}")
    val defaultTax: BigDecimal
) {
    private val TATUM_BASE_URL: String = "https://api-eu1.tatum.io/v3/"
    private fun getBuiltRetrofit(): Retrofit = RetrofitBuilder.getRetrofitBuilder().baseUrl(TATUM_BASE_URL).build()
    val tatumApiImpl: TatumApiInterface = getBuiltRetrofit().create(TatumApiInterface::class.java)
}

@EnableConfigurationProperties
interface TatumApiInterface {

    @GET("{currency}/wallet")
    fun generateCurrencyWallet(
        @Path("currency") currency: String,
        @Header("x-api-key") api_key: String
    ): Call<TatumWalletResponse>

    @GET("{currency}/address/{xpub}/1")
    fun generateWalletAddress(
        @Path("currency") currency: String,
        @Path("xpub") xpub: String,
        @Header("x-api-key") api_key: String
    ): Call<AddressResponse>

    @POST("{currency}/wallet/priv")
    fun generatePrivateKey(
        @Path("currency") currency: String,
        @Body privateKeyRequest: PrivateKeyRequest,
        @Header("x-api-key") api_key: String
    ): Call<PrivateKeyResponse>

    @POST("offchain/account/613c914ace422d545e41ca62/address/{address}")
    fun assignAddressToAccount(
        @Path("address") address: String,
        @Header("x-api-key") api_key: String
    ): Call<ResponseBody>

    @POST("{currency}/transaction")
    fun sendTransaction(
        @Path("currency") currency: String,
        @Body transactionRequest: TransactionRequest,
        @Header("x-api-key") api_key: String
    ): Call<ResponseBody>

}