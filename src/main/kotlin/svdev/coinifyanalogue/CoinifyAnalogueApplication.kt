package svdev.coinifyanalogue

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CoinifyAnalogueApplication

fun main(args: Array<String>) {
	runApplication<CoinifyAnalogueApplication>(*args)
}
