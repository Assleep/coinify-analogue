package svdev.coinifyanalogue.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import svdev.coinifyanalogue.common.entities.WalletEntity

interface WalletRepository : JpaRepository <WalletEntity, Long> {
    @Query("SELECT w FROM WalletEntity w WHERE w.currency =:currency")
    fun findOneByCurrency(@Param("currency") currency: String): WalletEntity?

    @Query("SELECT w FROM WalletEntity w WHERE w.address =:address")
    fun findByAddress(@Param("address") address: String): WalletEntity
}