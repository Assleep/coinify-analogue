package svdev.coinifyanalogue.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.transaction.annotation.Transactional
import svdev.coinifyanalogue.invoice.entities.InvoiceEntity

interface InvoiceRepository : JpaRepository<InvoiceEntity, Long> {
    @Query(
        """SELECT i FROM InvoiceEntity i WHERE i.status =:status
         AND i.targetWalletAddress =:targetWalletAddress"""
    )
    fun findOneByStatusAndTargetWalletAddress(
        @Param("status") status: String,
        @Param("targetWalletAddress") targetWalletAddress: String
    ): InvoiceEntity?

    @Transactional
    @Modifying
    @Query("UPDATE InvoiceEntity i SET i.status =:status WHERE i.businessKey =:businessKey")
    fun updateInvoiceStatus(@Param("businessKey") businessKey: String, @Param("status") status: String)

    @Query("SELECT i FROM InvoiceEntity i WHERE i.callback =:callback")
    fun findAllByCallback(@Param("callback") callback: String): List<InvoiceEntity>

    @Query("SELECT i FROM InvoiceEntity i WHERE i.businessKey =:businessKey")
    fun findByBusinessKey(@Param("businessKey") businessKey: String): InvoiceEntity?

}