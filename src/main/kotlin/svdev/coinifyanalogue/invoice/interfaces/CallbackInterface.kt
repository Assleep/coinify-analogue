package svdev.coinifyanalogue.invoice.interfaces

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface CallbackInterface {
    @POST
    @FormUrlEncoded
    fun sendResponse(
        @Field("customer_address") customer_address: String,
        @Field("status") status: String
    ): Call<ResponseBody>
}