package svdev.coinifyanalogue.invoice.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import svdev.coinifyanalogue.camunda.CamundaService
import svdev.coinifyanalogue.common.RetrofitBuilder
import svdev.coinifyanalogue.invoice.data.InvoiceRequest
import svdev.coinifyanalogue.invoice.data.InvoiceResponse
import svdev.coinifyanalogue.invoice.data.InvoiceStatus
import svdev.coinifyanalogue.invoice.data.Ticker
import svdev.coinifyanalogue.invoice.entities.InvoiceEntity
import svdev.coinifyanalogue.invoice.interfaces.CallbackInterface
import svdev.coinifyanalogue.repositories.InvoiceRepository
import svdev.coinifyanalogue.tatum.services.WalletService
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode
import java.util.*

@Service
class InvoiceService(
    @Autowired val invoiceRepository: InvoiceRepository,
    @Autowired val walletService: WalletService,
    @Autowired val camundaService: CamundaService
) {
    private fun generateInvoice(invoiceRequest: InvoiceRequest): InvoiceResponse {
        val targetWalletAddress = walletService.getAddress(Ticker.valueOf(invoiceRequest.currency))
        val finalAmount = calculateFinalAmount(invoiceRequest.amount)
        return InvoiceResponse(
            finalAmount,
            invoiceRequest.currency,
            invoiceRequest.customerAddress,
            targetWalletAddress,
            invoiceRequest.callback,
            InvoiceStatus.PENDING.name
        )
    }

    fun sendResponseToCallback(invoiceRequest: InvoiceRequest, invoiceStatus: InvoiceStatus): Boolean {
        val callback = RetrofitBuilder.getRetrofitBuilder().baseUrl(invoiceRequest.callback).build()
            .create(CallbackInterface::class.java)
        return callback.sendResponse(invoiceRequest.customerAddress, invoiceStatus.name).execute().isSuccessful
    }

    fun updateInvoiceStatus(businessKey: String, invoiceStatus: InvoiceStatus) {
        invoiceRepository.updateInvoiceStatus(
            businessKey, invoiceStatus.name
        )
    }

    fun findInvoice(status: InvoiceStatus, walletAddress: String): InvoiceEntity? =
        invoiceRepository.findOneByStatusAndTargetWalletAddress(
            InvoiceStatus.PENDING.name,
            walletAddress
        )

    fun sendInvoice(invoiceRequest: InvoiceRequest): InvoiceResponse {
        val businessKey = UUID.randomUUID()
        val invoiceResponse = generateInvoice(invoiceRequest)
        invoiceRepository.save(
            InvoiceEntity(
                invoiceRequest.amount,
                invoiceRequest.currency,
                invoiceRequest.customerAddress,
                invoiceResponse.targetWalletAddress,
                invoiceRequest.callback,
                businessKey.toString()
            )
        )
        invoiceRequest.amount = invoiceResponse.amount
        camundaService.startProcess(invoiceRequest, businessKey)
        return invoiceResponse
    }


    private fun calculateFinalAmount(requestAmount: BigDecimal): BigDecimal =
        requestAmount.plus(walletService.tatumRetrofit.defaultTax)
            .round(MathContext(2, RoundingMode.UP))

}