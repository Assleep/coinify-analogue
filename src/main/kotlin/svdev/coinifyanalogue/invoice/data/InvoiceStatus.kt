package svdev.coinifyanalogue.invoice.data

enum class InvoiceStatus {
    PENDING, CHECKING, CONFIRMED, CANCELED, REFUND
}