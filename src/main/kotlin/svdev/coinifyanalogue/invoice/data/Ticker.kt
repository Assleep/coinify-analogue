package svdev.coinifyanalogue.invoice.data

enum class Ticker(val fullName: String){
    ETH("ethereum"), BTC("bitcoin")
}