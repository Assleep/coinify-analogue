package svdev.coinifyanalogue.invoice.data

import spinjar.com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal

class InvoiceRequest(
    @JsonProperty("amount")
    var amount: BigDecimal,
    @JsonProperty("currency")
    val currency: String,
    @JsonProperty("customerAddress")
    val customerAddress: String,
    @JsonProperty("callback")
    val callback: String
)
