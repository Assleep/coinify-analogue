package svdev.coinifyanalogue.invoice.data

import java.math.BigDecimal

data class InvoiceResponse(
    val amount: BigDecimal,
    val currency: String,
    val customerAddress: String,
    val targetWalletAddress: String,
    val callback: String,
    var status: String? = "PENDING"
)
