package svdev.coinifyanalogue.invoice.entities

import java.math.BigDecimal
import javax.persistence.*

@Entity
@Table(name = "invoice")
data class InvoiceEntity(
    @Column
    val amount: BigDecimal,
    @Column
    val currency: String,
    @Column
    val customerAddress: String,
    @Column
    val targetWalletAddress: String,
    @Column
    val callback: String,
    @Column
    var businessKey: String = "",
    @Column
    var status: String? = "PENDING",
    @Id
    @GeneratedValue(
        strategy = GenerationType.IDENTITY
    )
    val id: Long? = null
)
