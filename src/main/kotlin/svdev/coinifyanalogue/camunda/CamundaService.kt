package svdev.coinifyanalogue.camunda

import org.camunda.bpm.engine.ProcessEngines
import org.camunda.bpm.engine.variable.Variables
import org.camunda.bpm.engine.variable.Variables.objectValue
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import svdev.coinifyanalogue.invoice.data.InvoiceRequest
import svdev.coinifyanalogue.invoice.data.InvoiceStatus
import svdev.coinifyanalogue.repositories.InvoiceRepository
import svdev.coinifyanalogue.tatum.data.WebhookResponse
import java.util.*

@Service
class CamundaService(@Autowired val invoiceRepository: InvoiceRepository) {
    companion object {
        const val processKey = "MainProcess"
    }

    fun startProcess(invoiceRequest: InvoiceRequest, businessKey: UUID) {
        val runtimeService = ProcessEngines.getDefaultProcessEngine().runtimeService
        runtimeService.createProcessInstanceByKey(processKey)
            .businessKey(businessKey.toString())
            .setVariable(
                "invoiceRequest",
                objectValue(invoiceRequest).serializationDataFormat(Variables.SerializationDataFormats.JSON).create()
            )
            .execute()
    }

    fun sendMessageToCamundaProcess(webhookResponse: WebhookResponse) {
        val runtimeService = ProcessEngines.getDefaultProcessEngine().runtimeService
        val invoice = invoiceRepository.findOneByStatusAndTargetWalletAddress(
            InvoiceStatus.PENDING.name,
            webhookResponse.to
        ) ?: return
        val processInstance = runtimeService.createProcessInstanceQuery()
            .processDefinitionKey(processKey)
            .active().processInstanceBusinessKey(invoice.businessKey).list()[0]

        val variables = mapOf(
            "invoicePaid" to objectValue(webhookResponse)
                .serializationDataFormat(Variables.SerializationDataFormats.JSON)
                .create(),
            "invoiceStatus" to InvoiceStatus.CHECKING
        )
        runtimeService.setVariables(
            runtimeService.createExecutionQuery()
                .processInstanceId(
                    processInstance.id
                ).list()[0].id, variables
        )
        runtimeService.correlateMessage("PaymentStatusUpdated", invoice.businessKey)
    }
}