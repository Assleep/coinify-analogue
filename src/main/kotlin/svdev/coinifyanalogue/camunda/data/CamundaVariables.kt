package svdev.coinifyanalogue.camunda.data

import io.holunda.camunda.bpm.data.CamundaBpmData.booleanVariable
import io.holunda.camunda.bpm.data.CamundaBpmData.customVariable
import io.holunda.camunda.bpm.data.factory.VariableFactory
import svdev.coinifyanalogue.invoice.data.InvoiceRequest
import svdev.coinifyanalogue.invoice.data.InvoiceStatus
import svdev.coinifyanalogue.tatum.data.WebhookResponse
import java.math.BigDecimal

object CamundaVariables {
    val INVOICE_REQUEST: VariableFactory<InvoiceRequest> = customVariable("invoiceRequest", InvoiceRequest::class.java)
    val INVOICE_PAID: VariableFactory<WebhookResponse> = customVariable("invoicePaid", WebhookResponse::class.java)
    val INVOICE_STATUS: VariableFactory<InvoiceStatus> = customVariable("invoiceStatus", InvoiceStatus::class.java)
    val RESPONSE_SENT: VariableFactory<Boolean> = booleanVariable("responseSent")
    val INTERMEDIATE_AMOUNT: VariableFactory<BigDecimal> = customVariable("intermediateAmount", BigDecimal::class.java)
}