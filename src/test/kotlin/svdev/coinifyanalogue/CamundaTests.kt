package svdev.coinifyanalogue

import org.camunda.bpm.engine.test.Deployment
import org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.assertThat
import org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.runtimeService
import org.camunda.bpm.engine.variable.Variables
import org.camunda.bpm.spring.boot.starter.test.helper.AbstractProcessEngineRuleTest
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import svdev.coinifyanalogue.invoice.data.InvoiceRequest
import svdev.coinifyanalogue.invoice.data.InvoiceStatus
import svdev.coinifyanalogue.tatum.data.WebhookResponse
import java.math.BigDecimal
import java.util.*


@SpringBootTest
@Deployment(resources = ["process.bpmn"])
class CamundaTests : AbstractProcessEngineRuleTest() {
    val processDefinitionKey = "MainProcess"

    @Test
    fun `should execute happy path`() {
        val businessKey = UUID.randomUUID()
        val invoiceRequest =
            InvoiceRequest(
                BigDecimal(0.02),
                "ETH",
                "0x0640e08a3718801fe4a10f4ddf761088e1002a3a",
                "test callback"
            )
        val webhookResponse = WebhookResponse(
            "0.02",
            "ETH",
            "0x0640e08a3718801fe4a10f4ddf761088e1002a3a",
            "address"
        )
        val processInstance =
            runtimeService().createProcessInstanceByKey(processDefinitionKey).businessKey(businessKey.toString())
                .setVariable(
                    "invoiceRequest",
                    Variables.objectValue(invoiceRequest)
                        .serializationDataFormat(Variables.SerializationDataFormats.JSON)
                        .create()
                ).execute()

        assertThat(processInstance).isStarted
            .hasPassed("GenerateInvoiceActivity")
            .isWaitingFor("PaymentStatusUpdated")

        runtimeService().setVariables(
            processInstance.id, mapOf(
                "invoicePaid" to Variables.objectValue(webhookResponse)
                    .serializationDataFormat(Variables.SerializationDataFormats.JSON)
                    .create(),
                "invoiceStatus" to InvoiceStatus.CHECKING
            )
        )

        runtimeService().correlateMessage("PaymentStatusUpdated", processInstance.businessKey)
        assertThat(processInstance)
            .hasPassed("WaitingForPaymentActivity")
            .hasPassed("CheckAmountActivity")
            .hasPassed("SendAmountToPayoutWalletActivity")
            .hasPassed("SendResponseToCallbackActivity")
            .isEnded
    }

    @Test
    fun `should execute path of insufficient payment`() {
        val businessKey = UUID.randomUUID()
        val invoiceRequest =
            InvoiceRequest(
                BigDecimal(0.02),
                "ETH",
                "0x0640e08a3718801fe4a10f4ddf761088e1002a3a",
                "test callback"
            )
        val firstWebhookResponse = WebhookResponse(
            "0.012",
            "ETH",
            "0x0640e08a3718801fe4a10f4ddf761088e1002a3a",
            "address"
        )
        val secondWebhookResponse = WebhookResponse(
            "0.01",
            "ETH",
            "0x0640e08a3718801fe4a10f4ddf761088e1002a3a",
            "address"
        )
        // invoiceRepository.save(InvoiceEntity(invoiceRequest.amount, "ETH", invoiceRequest.customerAddress, "address", "test callback", businessKey.toString()))
        val processInstance =
            runtimeService().createProcessInstanceByKey(
                "MainProcess"
            ).businessKey(businessKey.toString()).setVariable(
                "invoiceRequest",
                Variables.objectValue(invoiceRequest)
                    .serializationDataFormat(Variables.SerializationDataFormats.JSON)
                    .create()

            ).execute()

        assertThat(processInstance).isStarted
            .hasPassed("GenerateInvoiceActivity")
            .isWaitingFor("PaymentStatusUpdated")

        runtimeService().setVariables(
            processInstance.id, mapOf(
                "invoicePaid" to Variables.objectValue(firstWebhookResponse)
                    .serializationDataFormat(Variables.SerializationDataFormats.JSON)
                    .create(),
                "invoiceStatus" to InvoiceStatus.CHECKING
            )
        )

        runtimeService().correlateMessage("PaymentStatusUpdated", processInstance.businessKey)

        assertThat(processInstance)
            .hasPassed("WaitingForPaymentActivity")
            .hasPassed("CheckAmountActivity")
            .hasPassed("GenerateInvoiceActivity")
            .isWaitingFor("PaymentStatusUpdated")

        runtimeService().setVariables(
            processInstance.id, mapOf(
                "invoicePaid" to Variables.objectValue(secondWebhookResponse)
                    .serializationDataFormat(Variables.SerializationDataFormats.JSON)
                    .create(),
                "invoiceStatus" to InvoiceStatus.CHECKING
            )
        )

        runtimeService().correlateMessage("PaymentStatusUpdated", processInstance.businessKey)

        assertThat(processInstance)
            .hasPassed("WaitingForPaymentActivity")
            .hasPassed("CheckAmountActivity")
            .hasPassed("SendAmountToPayoutWalletActivity")
            .hasPassed("SendResponseToCallbackActivity")
            .isEnded
    }

    @Test
    fun `should execute the overpayment path`() {
        val businessKey = UUID.randomUUID()
        val invoiceRequest =
            InvoiceRequest(
                BigDecimal(0.02),
                "ETH",
                "0x0640e08a3718801fe4a10f4ddf761088e1002a3a",
                "test callback"
            )
        val webhookResponse = WebhookResponse(
            "0.032",
            "ETH",
            "0x0640e08a3718801fe4a10f4ddf761088e1002a3a",
            "address"
        )
        // invoiceRepository.save(InvoiceEntity(invoiceRequest.amount, "ETH", invoiceRequest.customerAddress, "address", "test callback", businessKey.toString()))
        val processInstance =
            runtimeService().createProcessInstanceByKey(
                "MainProcess"
            ).businessKey(businessKey.toString()).setVariable(
                "invoiceRequest",
                Variables.objectValue(invoiceRequest)
                    .serializationDataFormat(Variables.SerializationDataFormats.JSON)
                    .create()

            ).execute()

        assertThat(processInstance).isStarted
            .hasPassed("GenerateInvoiceActivity")
            .isWaitingFor("PaymentStatusUpdated")

        runtimeService().setVariables(
            processInstance.id, mapOf(
                "invoicePaid" to Variables.objectValue(webhookResponse)
                    .serializationDataFormat(Variables.SerializationDataFormats.JSON)
                    .create(),
                "invoiceStatus" to InvoiceStatus.CHECKING
            )
        )

        runtimeService().correlateMessage("PaymentStatusUpdated", processInstance.businessKey)

        assertThat(processInstance)
            .hasPassed("WaitingForPaymentActivity")
            .hasPassed("CheckAmountActivity")
            .hasPassed("SendAmountToPayoutWalletBeforeRefundActivity")
            .hasPassed("RefundActivity")
            .hasPassed("SendResponseToCallbackActivity")
            .isEnded
    }

}

