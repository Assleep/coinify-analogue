package svdev.coinifyanalogue

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.mockk.every
import io.mockk.justRun
import io.mockk.mockk
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import svdev.coinifyanalogue.invoice.data.Ticker
import svdev.coinifyanalogue.invoice.entities.InvoiceEntity
import svdev.coinifyanalogue.pay.entities.PayoutEntity
import svdev.coinifyanalogue.pay.repositories.PayoutRepository
import svdev.coinifyanalogue.pay.services.PayService
import svdev.coinifyanalogue.repositories.InvoiceRepository
import svdev.coinifyanalogue.tatum.services.WalletService
import java.io.File
import java.math.BigDecimal

@SpringBootTest
class PayServiceTest(
    @Autowired val walletService: WalletService,
    @Value("\${tatum.payoutWalletAddress}") val payoutWalletAddress: String
) {
    val objectMapper = ObjectMapper().registerKotlinModule()

    @Test
    fun `should make a payout`() {

        val invoiceRepository = mockk<InvoiceRepository>(relaxed = true)
        val payoutRepository = mockk<PayoutRepository>(relaxed = true)
        val walletService = mockk<WalletService>(relaxed = true)
        val payoutEntity = PayoutEntity(BigDecimal(0.02), "ETH", "toWalletAddress", "test callback")

        every { invoiceRepository.findAllByCallback(payoutEntity.callback) } returns objectMapper.readValue<List<InvoiceEntity>>(
            File("src/test/resources/TestPayins.json")
        ).filter { it.callback.equals(payoutEntity.callback) }

        every { payoutRepository.findAllByCallback(payoutEntity.callback) } returns objectMapper.readValue<List<PayoutEntity>>(
            File("src/test/resources/TestPayouts.json")
        ).filter { it.callback.equals(payoutEntity.callback) }

        every { payoutRepository.save(payoutEntity) } returns payoutEntity

        justRun { walletService.sendTransaction("toWalletAddress", Ticker.ETH, "0.02", "0x$payoutWalletAddress") }

        val payService = PayService(invoiceRepository, payoutRepository, walletService, payoutWalletAddress)

        assert(payService.makePayout(payoutEntity))
    }
}