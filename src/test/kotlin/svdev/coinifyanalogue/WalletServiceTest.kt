package svdev.coinifyanalogue

import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import svdev.coinifyanalogue.common.entities.WalletEntity
import svdev.coinifyanalogue.invoice.data.Ticker
import svdev.coinifyanalogue.repositories.WalletRepository
import svdev.coinifyanalogue.tatum.TatumRetrofit
import svdev.coinifyanalogue.tatum.data.AddressResponse
import svdev.coinifyanalogue.tatum.data.PrivateKeyRequest
import svdev.coinifyanalogue.tatum.data.PrivateKeyResponse
import svdev.coinifyanalogue.tatum.data.TatumWalletResponse
import svdev.coinifyanalogue.tatum.services.WalletService

@SpringBootTest
class WalletServiceTest(@Autowired val walletService: WalletService) {
    @Test
    fun `should generate wallet address`() {
        val walletRepository = mockk<WalletRepository>(relaxed = true)
        val tatumRetrofit = mockk<TatumRetrofit>(relaxed = true)
        every { walletRepository.findByAddress("address") } returns WalletEntity(
            "ETH",
            "xpub",
            "accountId",
            "address",
            "pkey"
        )
        every {
            walletRepository.save(
                WalletEntity(
                    "ETH",
                    "xpub",
                    "",
                    "address",
                    "pkey"
                )
            )
        } returns WalletEntity(
            "ETH",
            "xpub",
            "accountId",
            "address",
            "pkey"
        )

        every {
            tatumRetrofit.tatumApiImpl.generateCurrencyWallet("ethereum", "").execute().body()
        } returns TatumWalletResponse("xpub", "mnemonic")
        every {
            tatumRetrofit.tatumApiImpl.generateWalletAddress("ethereum", "xpub", "").execute().body()
        } returns AddressResponse("address")
        every {
            tatumRetrofit.tatumApiImpl.generatePrivateKey("ethereum", PrivateKeyRequest("mnemonic", 1), "").execute()
                .body()
        } returns PrivateKeyResponse("pkey")

        val injectedWalletService = WalletService(walletRepository, tatumRetrofit, "payoutWalletAddress")
        assertEquals("address", injectedWalletService.getAddress(Ticker.ETH))
    }

    @Test
    fun `should send transaction`() {
        walletService.sendTransaction("address", Ticker.ETH, "0.02", "test_wallet_address")
    }

}