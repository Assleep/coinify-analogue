import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.5.4"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	kotlin("jvm") version "1.5.21"
	kotlin("plugin.spring") version "1.5.21"
	kotlin("plugin.jpa") version "1.5.21"
}

group = "svdev"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}
dependencies {
	implementation("org.springframework.boot:spring-boot-starter-data-jdbc:2.5.4")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa:2.5.4")
	implementation("org.springframework.boot:spring-boot-starter-web:2.5.4")

	implementation("org.springframework.boot:spring-boot-starter-jdbc:2.5.4")
	implementation("org.jetbrains.kotlin:kotlin-reflect")

	implementation("org.camunda.bpm.springboot:camunda-bpm-spring-boot-starter-rest:7.15.0")
	implementation("org.camunda.bpm.springboot:camunda-bpm-spring-boot-starter-webapp:7.15.0")
	implementation("org.camunda.bpm.springboot:camunda-bpm-spring-boot-starter-test:7.15.0")
	implementation("org.camunda.bpm:camunda-engine-plugin-spin:7.16.0-alpha4")
	implementation("org.camunda.spin:camunda-spin-dataformat-all:1.11.0")
	implementation("io.holunda.data:camunda-bpm-data:1.2.3")

	implementation("com.h2database:h2:1.4.200")
	implementation("org.postgresql:postgresql:42.2.23.jre7")

	implementation("com.squareup.retrofit2:retrofit:2.9.0")
	implementation("com.squareup.retrofit2:converter-jackson:2.9.0")
	implementation("com.squareup.retrofit2:converter-scalars:2.9.0")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.12.5")

	testImplementation("org.springframework.boot:spring-boot-starter-test:2.5.4")
	testImplementation("io.mockk:mockk:1.12.0")

}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
